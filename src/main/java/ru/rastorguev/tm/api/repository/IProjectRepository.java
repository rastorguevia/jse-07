package ru.rastorguev.tm.api.repository;

import ru.rastorguev.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void removeAllByUserId(String userId);

    List<Project> findAllByUserId(String userId);

    String getProjectIdByNumber(final int number);
}
