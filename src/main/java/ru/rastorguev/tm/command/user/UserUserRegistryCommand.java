package ru.rastorguev.tm.command.user;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

public final class UserUserRegistryCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getName() {
        return "user_registry";
    }

    @Override
    public String getDescription() {
        return "Registry new User.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("User registry");
        System.out.println("Enter login");
        final String login = serviceLocator.getTerminalService().nextLine();
        System.out.println("Enter password");
        final String password = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getUserService().userRegistry(login, password);
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return null;
    }
}