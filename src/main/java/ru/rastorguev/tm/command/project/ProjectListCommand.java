package ru.rastorguev.tm.command.project;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import static ru.rastorguev.tm.view.View.*;

public final class ProjectListCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getName() {
        return "project_list";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        System.out.println("Project list");
        final User user = serviceLocator.getUserService().getCurrentUser();
        printAllProjectsForUser(serviceLocator.getProjectService().findAllByUserId(user.getId()));
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}