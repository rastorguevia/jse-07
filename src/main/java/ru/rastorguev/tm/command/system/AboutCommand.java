package ru.rastorguev.tm.command.system;

import com.jcabi.manifests.Manifests;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

public final class AboutCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "Build number info.";
    }

    @Override
    public void execute() throws IOException {
        final String projectVersion = Manifests.read("projectVersion");
        final String buildNumber = Manifests.read("buildNumber");
        final String developer = Manifests.read("developer");
        System.out.println("projectVersion: " + projectVersion + "\n" +"build number: " + buildNumber
                + "\n" + "developer: " + developer);
    }

    @Override
    public Role[] roles() {
        return null;
    }
}
