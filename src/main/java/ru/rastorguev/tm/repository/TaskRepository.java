package ru.rastorguev.tm.repository;

import ru.rastorguev.tm.api.repository.ITaskRepository;
import ru.rastorguev.tm.entity.Task;

import java.util.*;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void removeTaskListByProjectId(final String projectId) {
        final List<Task> listOfTasks = new LinkedList<>();
        listOfTasks.addAll(findAll());
        for (final Task task: listOfTasks) {
            if (task.getProjectId().equals(projectId)) {
                remove(task.getId());
            }
        }
    }

    @Override
    public List<Task> filterTaskListByProjectId(final String projectId, final Collection<Task> taskCollection) {
        final List<Task> listOfTasks = new LinkedList<>();
        listOfTasks.addAll(taskCollection);
        for (int i = 0; i < listOfTasks.size(); i++) {
            if (!listOfTasks.get(i).getProjectId().equals(projectId)) {
                listOfTasks.remove(i);
            }
        }
        return listOfTasks;
    }
}