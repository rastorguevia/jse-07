package ru.rastorguev.tm.command;

import ru.rastorguev.tm.api.service.ServiceLocator;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public ServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract boolean secure();
    public abstract String getName();
    public abstract String getDescription();
    public abstract void execute() throws IOException;
    public abstract Role[] roles();
}