package ru.rastorguev.tm.repository;

import ru.rastorguev.tm.api.repository.IRepository;
import ru.rastorguev.tm.entity.AbstractEntity;
import ru.rastorguev.tm.error.EntityDuplicateException;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final Map<String, E> map = new LinkedHashMap<>();

    @Override
    public Collection<E> findAll() {
        return map.values();
    }

    @Override
    public E findOne(final String entityId) {
        return map.get(entityId);
    }

    @Override
    public E persist(final E entity) {
        if (map.containsKey(entity.getId())) throw new EntityDuplicateException("Entity already exist");
        return map.put(entity.getId(), entity);
    }

    @Override
    public E merge(final E entity) {
        map.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public E remove(String entityId) {
        return map.remove(entityId);
    }

    @Override
    public void removeAll() {
        map.clear();
    }
}