package ru.rastorguev.tm.repository;

import ru.rastorguev.tm.api.repository.IUserRepository;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import java.util.LinkedList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public void userRegistry(final String login, final String password) {
        final User user = new User(login, password, Role.USER);
        persist(user);
    }

    @Override
    public void adminRegistry(final String login, final String password) {
        final User admin = new User(login, password, Role.ADMINISTRATOR);
        persist(admin);
    }

    @Override
    public User findByLogin(final String login) {
        final List<User> listOfUsers = new LinkedList<>();
        listOfUsers.addAll(findAll());
        for (final User user : listOfUsers) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    @Override
    public boolean isExistByLogin(final String login) {
        final List<User> listOfUsers = new LinkedList<>();
        listOfUsers.addAll(findAll());
        for (final User user : listOfUsers) {
            if (user.getLogin().equals(login)) return true;
        }
        return false;
    }
}