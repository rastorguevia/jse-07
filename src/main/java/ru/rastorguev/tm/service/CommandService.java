package ru.rastorguev.tm.service;

import ru.rastorguev.tm.api.service.ICommandService;
import ru.rastorguev.tm.command.AbstractCommand;

import java.util.Map;

public final class CommandService implements ICommandService {

    private final Map<String, AbstractCommand> commandMap;

    public CommandService(final Map<String, AbstractCommand> commandMap) {
        this.commandMap = commandMap;
    }

    public Map<String, AbstractCommand> getCommandMap() {
        return commandMap;
    }
}
