package ru.rastorguev.tm.entity;

import ru.rastorguev.tm.enumerated.Role;

import static ru.rastorguev.tm.util.MD5Util.mdHashCode;

public final class User extends AbstractEntity {
    private String login;
    private String passHash;
    private Role role;

    public User() {
    }

    public User(final Role role) {
        this.role = role;
    }

    public User(final String login, final String passHash, final Role role) {
        this.login = login;
        this.passHash = mdHashCode(passHash);
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getPassHash() {
        return passHash;
    }

    public void setPassHash(final String passHash) {
        this.passHash = mdHashCode(passHash);
    }

    public Role getRole() {
        return role;
    }

    public void setRole(final Role role) {
        this.role = role;
    }
}
