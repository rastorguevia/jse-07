package ru.rastorguev.tm.command.user;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Confirmation;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

import static ru.rastorguev.tm.view.View.*;

public final class UserCheckAndEditProfileCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getName() {
        return "check_edit";
    }

    @Override
    public String getDescription() {
        return "Check and edit current profile.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Check and edit");
        final User user = serviceLocator.getUserService().getCurrentUser();
        printUserProfile(user);
        System.out.println("Do you want to edit? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
            System.out.println("Enter new login");
            final String login = serviceLocator.getTerminalService().nextLine();
            if (serviceLocator.getUserService().isExistByLogin(login)) {
                System.out.println("This login already exist");
            } else user.setLogin(login);
            serviceLocator.getUserService().merge(user);
            System.out.println("OK");
        }
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}