package ru.rastorguev.tm;

import ru.rastorguev.tm.context.Bootstrap;

import java.io.IOException;

public final class Application {
    public static void main(final String[] args) throws Exception {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}