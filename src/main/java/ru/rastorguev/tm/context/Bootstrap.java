package ru.rastorguev.tm.context;

import static ru.rastorguev.tm.view.View.*;

import ru.rastorguev.tm.api.repository.IProjectRepository;
import ru.rastorguev.tm.api.repository.ITaskRepository;
import ru.rastorguev.tm.api.repository.IUserRepository;
import ru.rastorguev.tm.api.service.*;
import ru.rastorguev.tm.command.*;
import ru.rastorguev.tm.command.system.AboutCommand;
import ru.rastorguev.tm.command.system.ExitCommand;
import ru.rastorguev.tm.command.system.HelpCommand;
import ru.rastorguev.tm.command.project.*;
import ru.rastorguev.tm.command.task.*;
import ru.rastorguev.tm.command.user.*;
import ru.rastorguev.tm.repository.ProjectRepository;
import ru.rastorguev.tm.repository.TaskRepository;
import ru.rastorguev.tm.repository.UserRepository;
import ru.rastorguev.tm.service.*;

import java.util.LinkedHashMap;
import java.util.Map;

public final class Bootstrap implements ServiceLocator {
    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectRepository projectRepository = new ProjectRepository(taskRepository);
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IUserRepository userRepository = new UserRepository();
    private final IUserService userService = new UserService(userRepository);
    private final ITerminalService terminalService = new TerminalService();
    private final Map<String, AbstractCommand> commandMap = new LinkedHashMap<>();
    private final ICommandService commandService = new CommandService(commandMap);

    public ITaskService getTaskService() { return taskService; }

    public IProjectService getProjectService() {
        return projectService;
    }

    public IUserService getUserService() {
        return userService;
    }

    public ITerminalService getTerminalService() {
        return terminalService;
    }

    public ICommandService getCommandService() {
        return commandService;
    }

    public void init() throws Exception {
        commandRegistry(new HelpCommand());
        commandRegistry(new ProjectClearCommand());
        commandRegistry(new ProjectCreateCommand());
        commandRegistry(new ProjectEditCommand());
        commandRegistry(new ProjectListCommand());
        commandRegistry(new ProjectRemoveCommand());
        commandRegistry(new ProjectSelectCommand());
        commandRegistry(new TaskClearCommand());
        commandRegistry(new TaskCreateCommand());
        commandRegistry(new TaskEditCommand());
        commandRegistry(new TaskListCommand());
        commandRegistry(new TaskRemoveCommand());
        commandRegistry(new TaskSelectCommand());
        commandRegistry(new UserLogInCommand());
        commandRegistry(new UserLogOutCommand());
        commandRegistry(new UserUserRegistryCommand());
        commandRegistry(new UserAdminRegistryCommand());
        commandRegistry(new UserPasswordUpdateCommand());
        commandRegistry(new UserCheckAndEditProfileCommand());
        commandRegistry(new AboutCommand());
        commandRegistry(new ExitCommand());

        createUsers();

        launchConsole();
    }

    private void commandRegistry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandMap.put(command.getName(), command);
    }

    public void launchConsole() throws Exception {
        showWelcomeMsg();

        String input = "";
        while (true) {
            input = terminalService.nextLine().toLowerCase();
            execute(input);
        }
    }

    private void execute(final String input) throws Exception {
        if (input == null || input.isEmpty()) {
            showUnknownCommandMsg();
            return;
        }
        final AbstractCommand command = commandMap.get(input);
        if (command == null) {
            showUnknownCommandMsg();
            return;
        }
        final boolean secureCheck = !command.secure() || (command.secure() && userService.isAuth());
        final boolean roleCheck = command.roles() == null ||
                (command.roles() != null && userService.isRolesAllowed(command.roles()));
        if (secureCheck && roleCheck) command.execute();
        else showAccessDeniedMsg();
    }

    private void createUsers() {
        userService.userRegistry("user", "user123");
        userService.adminRegistry("admin", "admin123");
    }
}