package ru.rastorguev.tm.api.repository;

import ru.rastorguev.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    void removeTaskListByProjectId(String projectId);

    List<Task> filterTaskListByProjectId(final String projectId, final Collection<Task> taskCollection);
}