package ru.rastorguev.tm.error;

public final class EntityDuplicateException extends RuntimeException{

    public EntityDuplicateException(final String message) {
        super(message);
    }
}