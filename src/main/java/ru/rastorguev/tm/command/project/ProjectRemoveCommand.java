package ru.rastorguev.tm.command.project;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.context.Bootstrap;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

import static ru.rastorguev.tm.view.View.*;

public final class ProjectRemoveCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getName() {
        return "project_remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Project remove");
        System.out.println("Enter ID");
        final User user = serviceLocator.getUserService().getCurrentUser();
        printAllProjectsForUser(serviceLocator.getProjectService().findAllByUserId(user.getId()));
        final String projectId = serviceLocator.getProjectService().getProjectIdByNumberForUser(Integer.parseInt(serviceLocator.getTerminalService().nextLine()), user.getId());
        serviceLocator.getProjectService().remove(projectId);
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}