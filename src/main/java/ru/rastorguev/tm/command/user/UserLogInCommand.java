package ru.rastorguev.tm.command.user;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

public final class UserLogInCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getName() {
        return "log_in";
    }

    @Override
    public String getDescription() {
        return "User log in.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Log in");
        System.out.println("Enter login");
        final String login = serviceLocator.getTerminalService().nextLine();
        System.out.println("Enter password");
        final String password = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getUserService().logIn(login, password);
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return null;
    }
}
