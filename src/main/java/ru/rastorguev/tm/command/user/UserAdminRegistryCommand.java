package ru.rastorguev.tm.command.user;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

public final class UserAdminRegistryCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getName() {
        return "admin_registry";
    }

    @Override
    public String getDescription() {
        return "Registry new Admin.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Admin registry");
        System.out.println("Enter login");
        final String login = serviceLocator.getTerminalService().nextLine();
        System.out.println("Enter password");
        final String password = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getUserService().adminRegistry(login, password);
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR };
    }
}