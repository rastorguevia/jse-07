package ru.rastorguev.tm.command.task;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Confirmation;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

import static ru.rastorguev.tm.util.DateUtil.*;
import static ru.rastorguev.tm.view.View.*;

public final class TaskCreateCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getName() {
        return "task_create";
    }

    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Task create");
        System.out.println("Enter Project ID");
        final User user = serviceLocator.getUserService().getCurrentUser();
        printAllProjectsForUser(serviceLocator.getProjectService().findAllByUserId(user.getId()));
        final String projectId = serviceLocator.getProjectService().getProjectIdByNumberForUser(Integer.parseInt(serviceLocator.getTerminalService().nextLine()), user.getId());
        final Task task = new Task(projectId);
        System.out.println("Enter task name");
        task.setName(serviceLocator.getTerminalService().nextLine());
        System.out.println("Enter task description");
        task.setDescription(serviceLocator.getTerminalService().nextLine());
        System.out.println("Add date? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
            System.out.println("Add start date DD.MM.YYYY? Y/N");
            if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
                System.out.println("Enter start date");
                task.setStartDate(stringToDate(serviceLocator.getTerminalService().nextLine()));
            }
            System.out.println("Add end date DD.MM.YYYY? Y/N");
            if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
                System.out.println("Enter end date");
                task.setEndDate(stringToDate(serviceLocator.getTerminalService().nextLine()));
            }
        }
        serviceLocator.getTaskService().persist(task);
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}