package ru.rastorguev.tm.service;

import ru.rastorguev.tm.api.repository.IProjectRepository;
import ru.rastorguev.tm.api.repository.IRepository;
import ru.rastorguev.tm.api.service.IProjectService;
import ru.rastorguev.tm.entity.Project;

import java.util.LinkedList;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public IRepository getRepository() {
        return projectRepository;
    }

    @Override
    public List<Project> findAllByUserId(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public void removeAllByUserId(final String userId) {
        if (userId == null || userId.isEmpty()) return;
        projectRepository.removeAllByUserId(userId);
    }

    @Override
    public String getProjectIdByNumber(final int number) {
        return projectRepository.getProjectIdByNumber(number);
    }

    @Override
    public String getProjectIdByNumberForUser(final int number, final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        final List<Project> filteredListOfProjects = findAllByUserId(userId);
        return filteredListOfProjects.get(number - 1).getId();
    }
}