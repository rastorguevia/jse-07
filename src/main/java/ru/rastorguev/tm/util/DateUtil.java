package ru.rastorguev.tm.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtil {
    public final static SimpleDateFormat dateFormatter = new SimpleDateFormat("d.MM.y");

    public static Date stringToDate (final String date) {
        Date res = null;
        try {
            res = dateFormatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return res;
    }
}