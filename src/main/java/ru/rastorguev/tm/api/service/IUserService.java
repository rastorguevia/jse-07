package ru.rastorguev.tm.api.service;

import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

public interface IUserService extends IService<User> {

    User getCurrentUser();

    void userRegistry(String login, String password);

    void adminRegistry(String login, String password);

    User findByLogin(String login);

    boolean isExistByLogin(String login);

    boolean isAuth();

    boolean isRolesAllowed(Role... roles);

    void logIn(String login, String password);

    void logOut();
}
