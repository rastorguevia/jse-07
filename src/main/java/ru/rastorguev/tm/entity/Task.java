package ru.rastorguev.tm.entity;

import java.util.Date;

import static ru.rastorguev.tm.util.DateUtil.*;

public final class Task extends AbstractEntity {
    private final String projectId;
    private String name = "";
    private String description = "";
    private Date startDate = stringToDate(dateFormatter.format(new Date()));
    private Date endDate = stringToDate(dateFormatter.format(new Date()));

    public Task(final String projectId) {
        this.projectId = projectId;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(final Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(final Date endDate) {
        this.endDate = endDate;
    }
}