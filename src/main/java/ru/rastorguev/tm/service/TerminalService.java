package ru.rastorguev.tm.service;

import ru.rastorguev.tm.api.service.ITerminalService;

import java.util.Scanner;

public final class TerminalService implements ITerminalService {

    private final Scanner scanner = new Scanner(System.in);

    public String nextLine() {
        return scanner.nextLine();
    }
}