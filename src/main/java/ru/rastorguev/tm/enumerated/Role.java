package ru.rastorguev.tm.enumerated;

public enum Role {
    ADMINISTRATOR ("Administrator"),
    USER ("User");

    private final String displayName;

    Role(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}