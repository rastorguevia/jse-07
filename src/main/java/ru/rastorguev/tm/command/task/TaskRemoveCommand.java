package ru.rastorguev.tm.command.task;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;
import java.util.List;

import static ru.rastorguev.tm.view.View.*;

public final class TaskRemoveCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getName() {
        return "task_remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Task remove");
        System.out.println("Enter Project ID");
        final User user = serviceLocator.getUserService().getCurrentUser();
        printAllProjectsForUser(serviceLocator.getProjectService().findAllByUserId(user.getId()));
        final String projectId = serviceLocator.getProjectService().getProjectIdByNumberForUser(Integer.parseInt(serviceLocator.getTerminalService().nextLine()), user.getId());
        printTaskListByProjectId(projectId, serviceLocator.getTaskService().findAll());
        final List<Task> filteredTaskList = serviceLocator.getTaskService().filterTaskListByProjectId(projectId, serviceLocator.getTaskService().findAll());
        System.out.println("Enter Task ID");
        final String taskId = serviceLocator.getTaskService().getTaskIdByNumber(Integer.parseInt(serviceLocator.getTerminalService().nextLine()),filteredTaskList);
        serviceLocator.getTaskService().remove(taskId);
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}