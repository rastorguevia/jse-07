package ru.rastorguev.tm.command.project;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

import static ru.rastorguev.tm.view.View.*;

public final class ProjectSelectCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getName() {
        return "project_select";
    }

    @Override
    public String getDescription() {
        return "Select exact project.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Project select");
        System.out.println("Enter project ID");
        final User user = serviceLocator.getUserService().getCurrentUser();
        printAllProjectsForUser(serviceLocator.getProjectService().findAllByUserId(user.getId()));
        final String projectId = serviceLocator.getProjectService().getProjectIdByNumberForUser(Integer.parseInt(serviceLocator.getTerminalService().nextLine()), user.getId());
        final Project project = serviceLocator.getProjectService().findOne(projectId);
        printProject(project);
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}