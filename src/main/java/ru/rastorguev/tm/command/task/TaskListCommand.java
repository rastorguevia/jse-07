package ru.rastorguev.tm.command.task;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

import static ru.rastorguev.tm.view.View.*;

public final class TaskListCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getName() {
        return "task_list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Task list");
        System.out.println("Enter Project ID");
        final User user = serviceLocator.getUserService().getCurrentUser();
        printAllProjectsForUser(serviceLocator.getProjectService().findAllByUserId(user.getId()));
        final String projectId = serviceLocator.getProjectService().getProjectIdByNumberForUser(Integer.parseInt(serviceLocator.getTerminalService().nextLine()), user.getId());
        printTaskListByProjectId(projectId, serviceLocator.getTaskService().findAll());
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}