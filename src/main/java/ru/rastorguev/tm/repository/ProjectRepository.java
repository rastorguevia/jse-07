package ru.rastorguev.tm.repository;

import ru.rastorguev.tm.api.repository.IProjectRepository;
import ru.rastorguev.tm.api.repository.ITaskRepository;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.Task;

import java.util.*;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    private final ITaskRepository taskRepository;

    public ProjectRepository(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Project remove(final String projectId) {
        final List<Task> taskList = new LinkedList<>();
        taskList.addAll(taskRepository.findAll());
        for (final Task task: taskList) {
            if (task.getProjectId().equals(projectId)) {
                taskRepository.remove(task.getId());
            }
        }
        return map.remove(projectId);
    }

    @Override
    public void removeAll() {
        map.clear();
        taskRepository.removeAll();
    }

    @Override
    public List<Project> findAllByUserId(final String userId) {
        final List<Project> listOfProjects = new LinkedList<>();
        listOfProjects.addAll(findAll());
        final List<Project> filteredListOfProjects = new LinkedList<>();
        for (final Project project : listOfProjects) {
            if (project.getUserId().equals(userId)) {
                filteredListOfProjects.add(project);
            }
        }
        return filteredListOfProjects;
    }

    @Override
    public void removeAllByUserId(final String userId) {
        final List<Project> listOfProjects = new LinkedList<>();
        listOfProjects.addAll(findAll());
        for (final Project project : listOfProjects) {
            if (project.getUserId().equals(userId)) {
                remove(project.getId());
            }
        }
    }

    @Override
    public String getProjectIdByNumber(final int number) {
        final List<Project> listOfProjects = new LinkedList<>();
        listOfProjects.addAll(findAll());
        return listOfProjects.get(number - 1).getId();
    }
}