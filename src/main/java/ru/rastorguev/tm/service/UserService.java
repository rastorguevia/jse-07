package ru.rastorguev.tm.service;

import ru.rastorguev.tm.api.repository.IRepository;
import ru.rastorguev.tm.api.repository.IUserRepository;
import ru.rastorguev.tm.api.service.IUserService;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static ru.rastorguev.tm.util.MD5Util.mdHashCode;

public final class UserService extends AbstractService<User> implements IUserService {

    private User currentUser = null;

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public IRepository getRepository() {
        return userRepository;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void userRegistry(final String login, final String password) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (isExistByLogin(login)) return;
        userRepository.userRegistry(login, password);
    }

    @Override
    public void adminRegistry(final String login, final String password) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (isExistByLogin(login)) return;
        userRepository.adminRegistry(login, password);
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean isExistByLogin(final String login) {
        if (login == null || login.isEmpty()) return false;
        return userRepository.isExistByLogin(login);
    }

    @Override
    public boolean isAuth() {
        return currentUser != null;
    }

    @Override
    public boolean isRolesAllowed(final Role... roles) {
        if (roles == null) return false;
        if (currentUser == null || currentUser.getRole() == null) return false;
        final List<Role> listOfRoles = Arrays.asList(roles);
        return listOfRoles.contains(currentUser.getRole());
    }

    @Override
    public void logIn(final String login, final String password) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        final User user = findByLogin(login);
        if (user == null) return;
        if (user.getPassHash().equals(mdHashCode(password))) {
            currentUser = user;
        }
    }

    @Override
    public void logOut() {
        currentUser = null;
    }
}