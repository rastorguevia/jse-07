package ru.rastorguev.tm.service;

import ru.rastorguev.tm.api.repository.IRepository;
import ru.rastorguev.tm.api.repository.ITaskRepository;
import ru.rastorguev.tm.api.service.ITaskService;
import ru.rastorguev.tm.entity.Task;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public IRepository getRepository() {
        return taskRepository;
    }

    @Override
    public void removeTaskListByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        taskRepository.removeTaskListByProjectId(projectId);
    }

    @Override
    public List<Task> filterTaskListByProjectId(final String projectId, final Collection<Task> taskCollection) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskCollection == null) return null;
        return taskRepository.filterTaskListByProjectId(projectId, taskCollection);
    }

    @Override
    public String getTaskIdByNumber(final int number, final List<Task> filteredListOfTasks) {
        if (filteredListOfTasks == null) return null;
        for (final Task task: filteredListOfTasks) {
            if (task.getId().equals(filteredListOfTasks.get(number - 1).getId())) {
                return task.getId();
            }
        }
        return null;
    }
}