package ru.rastorguev.tm.command.system;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.enumerated.Role;

import java.util.LinkedList;
import java.util.List;

public final class HelpCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() { return "Show all commands."; }

    @Override
    public void execute() {
        final List<AbstractCommand> AbstractCommandList = new LinkedList<>(serviceLocator.getCommandService().getCommandMap().values());
        for (final AbstractCommand command : AbstractCommandList) {
            System.out.println(command.getName() + ": " + command.getDescription());
        }
    }

    @Override
    public Role[] roles() {
        return null;
    }
}